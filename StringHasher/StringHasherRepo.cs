﻿using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Security.Cryptography;

namespace StringHasher
{
    public class StringHasherRepo : IStringHasher
    {
        private readonly ILogger _logger;
        private const int SaltSize = 16; // 128 bit
        private const int KeySize = 32; // 256 bit

        public StringHasherRepo(ILogger<StringHasherRepo> logger)
        {
            _logger = logger;
        }

        public string CreateHash(string str, int iterations)
        {
            try
            {
                using (var algorithm = new Rfc2898DeriveBytes(
                  str,
                  SaltSize,
                  iterations,
                  HashAlgorithmName.SHA512))
                {
                    var key = Convert.ToBase64String(algorithm.GetBytes(KeySize));
                    var salt = Convert.ToBase64String(algorithm.Salt);

                    return $"{iterations}.{salt}.{key}";
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"StringHasher.CreateHash Error: {e.Message} ");
                throw;
            }
        }

        public bool ValidateHash(string hash, string str)
        {
            try
            {
                var parts = hash.Split('.', 3);

                if (parts.Length != 3)
                {
                    throw new FormatException("Unexpected hash format. " +
                      "Should be formatted as `{iterations}.{salt}.{hash}`");
                }

                var iterations = Convert.ToInt32(parts[0]);
                var salt = Convert.FromBase64String(parts[1]);
                var key = Convert.FromBase64String(parts[2]);

                using (var algorithm = new Rfc2898DeriveBytes(
                  str,
                  salt,
                  iterations,
                  HashAlgorithmName.SHA512))
                {
                    var keyToCheck = algorithm.GetBytes(KeySize);

                    var verified = keyToCheck.SequenceEqual(key);

                    return (verified);
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"StringHasher.ValidateHash Error: {e.Message} ");
                throw;
            }
        }
    }
}