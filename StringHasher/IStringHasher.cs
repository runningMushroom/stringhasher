﻿namespace StringHasher
{
    public interface IStringHasher
    {
        string CreateHash(string str, int iterations);

        bool ValidateHash(string hash, string str);
    }
}