﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace StringHasher
{
    public static class StringHasherExtension
    {
        public static void ConfigureStringHasher(this IServiceCollection services)
        {
            services.AddScoped<IStringHasher, StringHasherRepo>();
        }
    }
}