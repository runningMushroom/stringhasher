# Functionality: 
* CreateHash:
	* Returns has of given string
* ValidateHash:
	* Return true/false on a given string & hashed string

# Target framework
* netcoreapp3.1

# Instructions
* Add services.ConfigureStringHasher() to your Startup.cs file
